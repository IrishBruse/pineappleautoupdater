namespace PineappleUpdater;

using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text.Json;

public partial class Downloader
{
    public string Error { get; set; } = "";
    public int BytesDownloaded { get; set; }
    public string NewVersion { get; set; } = "";
    public int BytesTotal { get; set; }

    public State State { get; set; } = State.Waiting;

    private const string GithubApiUri = "https://api.github.com/repos/pineappleEA/pineapple-src/releases/latest";
    private const string VersionPath = "./version.txt";

    public async void Download()
    {
        foreach (Process item in Process.GetProcessesByName("yuzu"))
        {
            item.Kill();
        }

        string versionString = File.Exists(VersionPath) ? File.ReadAllText(VersionPath) : "0";

        int currentVersion = int.Parse(versionString);

        HttpClient http = new()
        {
            DefaultRequestHeaders =
            {
                { "User-Agent", "PineappleUpdater" }
            },
        };

        HttpResponseMessage resp = await http.GetAsync(GithubApiUri);

        if (resp.StatusCode != System.Net.HttpStatusCode.OK)
        {
            State = State.Error;
            Error = "Network Error: Failed to get latest release.";
            return;
        }

        string content = await resp.Content.ReadAsStringAsync();
        GitHubRelease? json = JsonSerializer.Deserialize(content, SourceGenerationContext.Default.GitHubRelease);

        if (json is null)
        {
            State = State.Error;
            Error = "Json Error: Failed to deserialize JSON.";
            return;
        }

        Uri zipUrl = null!;
        string zipName = "";
        foreach (Asset item in json.Assets)
        {
            const string windowsContentType = "application/zip";
            if (item.ContentType == windowsContentType)
            {
                BytesTotal = (int)item.Size;
                NewVersion = Path.GetFileNameWithoutExtension(item.Name)[^4..];
                zipName = item.Name;
                zipUrl = item.BrowserDownloadUrl;
                break;
            }
        }

        if (zipUrl is null || string.IsNullOrEmpty(zipName))
        {
            State = State.Error;
            Error = "GitHub API Error: Failed to find Windows build in Releases.";
            return;
        }

        if (!int.TryParse(NewVersion, out int version) || version > currentVersion)
        {
            using MemoryStream zipStream = new();

            State = State.Downloading;
            await http.DownloadDataAsync(zipUrl, zipStream, (progress) =>
            {
                BytesDownloaded = (int)progress;
            });

            BytesDownloaded = BytesTotal;

            using ZipArchive zip = new(zipStream);

            State = State.Unziping;

            try
            {
                zip.ExtractToDirectory("./", true);
            }
            catch (Exception)
            {
                State = State.Error;
                Error = "Zip Error: Failed to extract zip.";
                return;
            }
        }
        State = State.Launching;

        string exe = "./yuzu-windows-msvc-early-access/yuzu.exe";

        if (File.Exists(exe))
        {
            Process.Start(exe);
        }
        else
        {
            State = State.Error;
            Error = "Unknown Error: Exe is somehow not there?.";
            return;
        }

        File.WriteAllText(VersionPath, NewVersion);

        State = State.Done;
    }
}

public enum State
{
    Waiting,
    Downloading,
    Unziping,
    Launching,
    Done,
    Error,
}

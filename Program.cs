﻿namespace PineappleUpdater;

using System;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Threading;

using Raylib_CsLo;

public class Program
{

    private static volatile bool done;
    private static readonly Color Fill = new(22, 125, 240, 255);

    private static Font fontTtf;
    private static Font bigfontTtf;
    private static Downloader downloader = new();
    private static string status = "";
    private static float closeTimer;

    public static void Main()
    {
        Raylib.SetConfigFlags(ConfigFlags.FLAG_MSAA_4X_HINT);
        Raylib.InitWindow(600, 300, "Pineapple EA Updater");
        Raylib.SetTargetFPS(30);
        Raylib.SetExitKey(0);

        LoadContent();

        Thread downloadThread = new(downloader.Download);
        downloadThread.Start();

        while (!Raylib.WindowShouldClose()) // Detect window close button or ESC key
        {
            Raylib.BeginDrawing();
            {
                if (Update())
                {
                    return;
                }

                Draw();
            }
            Raylib.EndDrawing();
        }

        downloadThread.Interrupt();

        Raylib.CloseWindow();
    }

    private static unsafe void LoadContent()
    {
        Assembly assembly = Assembly.GetExecutingAssembly();
        using BinaryReader fontStream = new(assembly.GetManifestResourceStream("PineappleUpdater...OpenSans.ttf")!);

        int length = (int)fontStream.BaseStream.Length;
        byte[] data = fontStream.ReadBytes(length);

        fixed (byte* dataptr = data)
        {
            fontTtf = Raylib.LoadFontFromMemory(".ttf", dataptr, length, 24, null, 0);
            bigfontTtf = Raylib.LoadFontFromMemory(".ttf", dataptr, length, 32, null, 0);
        }

        using BinaryReader iconStream = new(assembly.GetManifestResourceStream("PineappleUpdater...icon.png")!);
        data = iconStream.ReadBytes((int)iconStream.BaseStream.Length);

        fixed (byte* dataprt = data)
        {
            Raylib.SetWindowIcon(Raylib.LoadImageFromMemory(".png", dataprt, data.Length));
        }
    }

    private static bool Update()
    {
        switch (downloader.State)
        {
            case State.Error:
            status = downloader.Error;
            break;

            case State.Waiting:
            status = "Please wait...";
            break;

            case State.Unziping:
            status = "Unzipping...";
            break;

            case State.Downloading:
            status = $"Downloading EA Version {downloader.NewVersion} ({SizeSuffix(downloader.BytesDownloaded)}/{SizeSuffix(downloader.BytesTotal)})";
            break;

            case State.Launching:
            status = "Launching Yuzu...";
            break;

            case State.Done:
            status = "Launching Yuzu...";
            closeTimer += Raylib.GetFrameTime();
            if (closeTimer > 1.5f)
            {
                Raylib.CloseWindow();
                return true;
            }
            break;
        }

        return false;
    }

    private static void Draw()
    {
        Raylib.ClearBackground(new Color(36, 36, 36, 255));
        Raylib.DrawTextPro(bigfontTtf, "Checking for updates...", new Vector2(44, 50), Vector2.Zero, 0, 32, 0, Raylib.WHITE);
        Raylib.DrawTextPro(fontTtf, status, new(44, 163), Vector2.Zero, 0, 24, 0, Raylib.WHITE);

        float percent = downloader.BytesDownloaded / (float)downloader.BytesTotal;
        if (downloader.State == State.Launching || downloader.State == State.Done)
        {
            percent = 1;
        }

        int width = Raylib.GetScreenWidth() - 88 - 20 + 2;
        Raylib.DrawRectangleRounded(new Rectangle(44, 189, Raylib.GetScreenWidth() - 88, 20), 1f, 5, Raylib.WHITE);
        Raylib.DrawRectangle(43 + 10, 189, (int)(width * percent), 20, Fill);

        if (downloader.State == State.Downloading || downloader.State == State.Unziping || downloader.State == State.Launching || downloader.State == State.Done)
        {
            Raylib.DrawCircleSector(new(43 + 10, 189 + 10), 10, 180, 360, 8, Fill);
        }

        if (downloader.State == State.Launching || downloader.State == State.Done)
        {
            Raylib.DrawCircleSector(new(43 + 10 + width, 189 + 10), 10, 0, 180, 8, Fill);
        }
    }

    private static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

    private static string SizeSuffix(long value, int decimalPlaces = 1)
    {
        if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException(nameof(decimalPlaces)); }
        if (value < 0) { return "-" + SizeSuffix(-value, decimalPlaces); }
        if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

        // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
        int mag = (int)Math.Log(value, 1024);

        // 1L << (mag * 10) == 2 ^ (10 * mag)
        // [i.e. the number of bytes in the unit corresponding to mag]
        decimal adjustedSize = (decimal)value / (1L << (mag * 10));

        // make adjustment when the value is large enough that
        // it would round up to 1000 or more
        if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
        {
            mag += 1;
            adjustedSize /= 1024;
        }

        return string.Format("{0:n" + decimalPlaces + "} {1}", adjustedSize, SizeSuffixes[mag]);
    }

}

namespace PineappleUpdater;

using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

public static class HttpClientProgressExtensions
{
    public static async Task DownloadDataAsync(this HttpClient client, Uri requestUrl, Stream destination, Action<long> progress, CancellationToken cancellationToken = default)
    {
        using HttpResponseMessage response = await client.GetAsync(requestUrl, HttpCompletionOption.ResponseHeadersRead, cancellationToken);
        long? contentLength = response.Content.Headers.ContentLength;
        using Stream download = await response.Content.ReadAsStreamAsync(cancellationToken);
        await download.CopyToAsync(destination, 81920, progress, cancellationToken);
    }

    private static async Task CopyToAsync(this Stream source, Stream destination, int bufferSize, Action<long> progress, CancellationToken cancellationToken = default)
    {
        byte[] buffer = new byte[bufferSize];
        long totalBytesRead = 0;
        int bytesRead;
        while ((bytesRead = await source.ReadAsync(buffer, cancellationToken).ConfigureAwait(false)) != 0)
        {
            await destination.WriteAsync(buffer.AsMemory(0, bytesRead), cancellationToken).ConfigureAwait(false);
            totalBytesRead += bytesRead;
            progress.Invoke(totalBytesRead);
        }
    }
}
